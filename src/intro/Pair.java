package intro;

import java.util.Scanner;

public class Pair {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in) ;	// ouverture du Scanner pour lire la console
		System.out.println("Entrez un nombre : ");		
		int nombre = scanner.nextInt();
		boolean pair = (nombre % 2 == 0 ? true : false);
		int reste = nombre % 2 ; // le reste de l'op�ration : nombre / 2 
		
		System.out.println("Le nombre "+nombre+( pair ?" est pair":" est impair"));
		System.out.println("Le reste de la division enti�re  "+reste);
		scanner.close();
	}

}
