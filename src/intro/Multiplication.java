package intro;

import java.util.Scanner;

public class Multiplication {

	public static void main(String[] args) {
		
		System.out.println("Quelle table voulez-vous calculer : ?");
		Scanner scanner = new Scanner(System.in) ;
		
		int rep = scanner.nextInt();
		System.out.println("Calcule de la table des :"+rep);
		
		for (int i = 0; i <= 10 ; i++) {
			System.out.println(i+" X "+rep+" ="+i*rep);
		}
		
		scanner.close();
	}
}
