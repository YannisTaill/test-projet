package intro;

public class SplitString {

	public static void main(String[] args) {
		String name ="Le Premier;Le 2;Le trois;4:La 5 partie|Derni�re partie" ;
		String[] result = name.split(";|:|\\|"); // Split avec 3 caract�res : ; : |
		
		for(int i=0;i<result.length;i++) {
			System.out.println("part["+i+"]="+result[i]);
		}
		
		System.out.println("Name est compos�e de "+result.length+" partie(s)");
		System.out.println("le caract�re : \" ");
	}
}
