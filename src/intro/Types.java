package intro;

/**
 * Classe de présentation des principaux types Java 
 * 
 * @author Developpeur
 *
 */
public class Types {

	public static void main(String[] args) {
		// Présentation des types Numériques (natifs/primitifs)
		int integerVariable = 102 ;
		long longVariable = 10_000_000 ;
		System.out.println("Valeur de integer 1 : "+integerVariable);
		System.out.println("Valeur de long  : "+longVariable);
		
		// integerVariable = integerVariable + 4;
		integerVariable += 4; // Instruction Equivalente 
		System.out.println("Valeur de integer 2 : "+integerVariable);
		// Incrementation 
		integerVariable++; // post-incrementation		
		System.out.println("Valeur de integer 3 : "+ --integerVariable);   // pre-decrementation 
		System.out.println("Valeur de integer finale : "+integerVariable);
		float floatVariable = 1.1f ;
		System.out.println("Valeur de float  : "+floatVariable);
		
		double doubleVariable = 1111.66d;
		System.out.println("Valeur de double  : "+doubleVariable);
		
		// Présentation des types Chaîne/Caractères (natifs)
		char charVariable = 'A' ;
		System.out.println("Valeur de char  : "+charVariable);
		
		// La casse est importante en Java
		String stringVariable = "Ma chaîne" ;
		System.out.println("Valeur de chaine (String)   : "+stringVariable);
		
		// Présentation du type Boolean
		
		boolean booleanVariable = false;
		System.out.println("Valeur de booléenne 1 : "+booleanVariable);
		booleanVariable = ! booleanVariable; // Utilisation du NOT (!)
		System.out.println("Valeur de booléenne 2 : "+booleanVariable);
	}
}
