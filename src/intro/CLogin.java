package intro;

import java.util.Scanner;

/**
 * Exemple de manipulation de cha�ne de caract�res
 * Version : 2 - Traite les noms et les pr�noms compos�s
 * 				 � l'aide de la m�thode split()
 * 
 * @author Developpeur
 *
 */
public class CLogin {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in) ;	// ouverture du Scanner pour lire la console
		System.out.println("Entrez votre pr�nom : ");		
		String firstName = scanner.nextLine();			// Lire les r�ponses de l'utilisateur
		
		System.out.println("Entrez votre nom : ");
		String lastName = scanner.nextLine();
		
//		String login = firstName.substring(0,1).toUpperCase()
//					 + lastName.substring(0,1).toUpperCase()
//					 + lastName.substring(1).toLowerCase();
		
		System.out.println("Pr�nom(s) : "+firstName);
		System.out.println("Nom(s) : "+lastName);
		
		String login = "";
		// String[] t = "Jean louis"
		String[] tfn = firstName.split(" |-");
		for(int i=0;i<tfn.length;i++) {
			login += tfn[i].substring(0,1).toUpperCase();
		}
		
		// "DURAND-DUPONT -> Durantdupont"
		tfn = lastName.split(" |-");
		for(int i=0;i<tfn.length;i++) {
			if ( i == 0)
				login += tfn[i].substring(0,1).toUpperCase()
					  +  tfn[i].substring(1).toLowerCase();
			else
				login += tfn[i].toLowerCase();
		}
		
		System.out.println("Login :"+login);
		scanner.close();

	}

}
