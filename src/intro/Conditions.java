package intro;

import java.util.Scanner;

public class Conditions {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); // ouverture du Scanner pour lire la console

		// �ge du sportif   
		System.out.println("Entrez votre �ge :  "); // Demande � l'utilisateur d'entrer son �ge
		int age = scanner.nextInt(); // Lire les r�ponses de l'utilisateur

		// Cat�gorie S�nior
		if (age > 18) {
			System.out.println("Vous �voluerez dans la cat�gorie S�nior. ");

		// Cat�gorie Junior
		} else if (age >= 16) {
			System.out.println("Vous �voluerez dans la cat�gorie Junior. ");
 
		// Cat�gorie Cadet
		} else if (age >= 12) {
			System.out.println("Vous �voluerez dans la cat�gorie Cadet. ");

		// Cat�gorie Minimes
		} else if (age >= 10) {
			System.out.println("Vous �voluerez dans la cat�gorie Minimes. ");

		// Cat�gorie Pupilles
		} else if (age >= 8) {
			System.out.println("Vous �voluerez dans la cat�gorie Pupilles. ");

		// Cat�gorie Poussin
		} else if (age >= 6) {
			System.out.println("Vous �voluerez dans la cat�gorie Poussin. ");
			
		// En dessous de 6 ans.
		} else {
			System.out.println("Trop jeune pour faire partie d'une cat�gorie. ");
		}

		scanner.close();
	}
}