package intro;

import java.util.Scanner;

public class Operator {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in) ;    // ouverture du Scanner pour lire la console
		
		// Entier X
		System.out.println("Entrez l'entier X : ");	  // Demande � l'utilisateur d'entrer X
		int entierX = scanner.nextInt();			  // Lire les r�ponses de l'utilisateur
		
		// Entier Y
		System.out.println("Entrez l'entier Y : ");   // Demande � l'utilisateur d'entrer Y
		int entierY = scanner.nextInt();			  // Lire les r�ponses de l'utilisateur
		
		// Somme de X + Y 
		System.out.println("La somme de X + Y = "+ (entierX + entierY));
		// Produit de X * Y
		System.out.println("Le produit de X * Y = "+ (entierX * entierY));

		// Diff�rence X et Y
		System.out.println("La diff�rence entre X et Y = "+ (entierX - entierY));
		// Quotient X et Y
		System.out.println("Le quotient de X / Y ="+ (entierX / entierY));
		// Le reste de X / Y 
		System.out.println("Le reste de X / Y= "+ (entierX % entierY));
		scanner.close();
	}
}

