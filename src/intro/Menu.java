package intro;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in); // ouverture du Scanner pour lire la console
		/**  Exercice 1 **/

		System.out.println("1) Ouvrir");
		System.out.println("2) Sauver");
		System.out.println("3) Quitter");

		
		int choix = scanner.nextInt(); // Lire les réponses de l'utilisateur

		switch (choix) {
		case 1:
			System.out.println("Vous avez choisi d'ouvrir");
			break;

		case 2:
			System.out.println("Vous avez choisi de sauver");
			break;

		case 3:
			System.out.println("Vous avez choisi de quitter");
			break;
		default:
			System.out.println("Valeur non valable");

		}
		
		
		/** Exercice 2 
		System.out.println("Entrer votre abréviation: ");	
		String abreviation = scanner.nextLine().toLowerCase(); // Lire les réponses de l'utilisateur et la convertir en minuscule
		
		switch (abreviation) {
		// Abréviation pour monsieur
		case "mr":
		case "m":
			System.out.println(abreviation + " = Monsieur");		
		break;
		//  Abréviation pour madame 
		case "mme":
			System.out.println(abreviation + " = Madame");
		break;
		// Abréviation pour mademoiselle
		case "mlle":
			System.out.println(abreviation + " = Mademoiselle");
		break;
		default:
			System.out.println("Abréviation non reconnue");		
		}
		**/
		
		scanner.close();
	}
}